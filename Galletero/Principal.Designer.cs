﻿namespace Galletero
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.btnCerrar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnMinimizar = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bnfControl = new Bunifu.Framework.UI.BunifuCards();
            this.lblEtapa = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.btnAvisoDetenida = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnAvisoActiva = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label12 = new System.Windows.Forms.Label();
            this.lblProceso = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.BarProceso = new Bunifu.Framework.UI.BunifuProgressBar();
            this.label6 = new System.Windows.Forms.Label();
            this.barTotal = new Bunifu.Framework.UI.BunifuProgressBar();
            this.btnTipo = new Bunifu.Framework.UI.BunifuCards();
            this.label1 = new System.Windows.Forms.Label();
            this.lblMarias = new System.Windows.Forms.Label();
            this.checkMarias = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lblBombon = new System.Windows.Forms.Label();
            this.checkBombon = new Bunifu.Framework.UI.BunifuCheckbox();
            this.lblChispas = new System.Windows.Forms.Label();
            this.checkChispas = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTiempo = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.txtCantidad = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bnfDatos = new Bunifu.Framework.UI.BunifuCards();
            this.btnReinicio = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnParo = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnInicio = new Bunifu.Framework.UI.BunifuFlatButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.bnfControl.SuspendLayout();
            this.btnTipo.SuspendLayout();
            this.bnfDatos.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCerrar
            // 
            this.btnCerrar.Activecolor = System.Drawing.Color.Red;
            this.btnCerrar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(158)))));
            this.btnCerrar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnCerrar.BorderRadius = 0;
            this.btnCerrar.ButtonText = "";
            this.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCerrar.DisabledColor = System.Drawing.Color.Gray;
            this.btnCerrar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnCerrar.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnCerrar.Iconimage")));
            this.btnCerrar.Iconimage_right = null;
            this.btnCerrar.Iconimage_right_Selected = null;
            this.btnCerrar.Iconimage_Selected = null;
            this.btnCerrar.IconMarginLeft = 0;
            this.btnCerrar.IconMarginRight = 0;
            this.btnCerrar.IconRightVisible = true;
            this.btnCerrar.IconRightZoom = 0D;
            this.btnCerrar.IconVisible = true;
            this.btnCerrar.IconZoom = 60D;
            this.btnCerrar.IsTab = false;
            this.btnCerrar.Location = new System.Drawing.Point(762, 2);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(158)))));
            this.btnCerrar.OnHovercolor = System.Drawing.Color.Red;
            this.btnCerrar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnCerrar.selected = false;
            this.btnCerrar.Size = new System.Drawing.Size(35, 33);
            this.btnCerrar.TabIndex = 0;
            this.btnCerrar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrar.Textcolor = System.Drawing.Color.White;
            this.btnCerrar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrar.Click += new System.EventHandler(this.btnCerrar_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(158)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btnMinimizar);
            this.panel1.Controls.Add(this.btnCerrar);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 37);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(3, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 29);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // btnMinimizar
            // 
            this.btnMinimizar.Activecolor = System.Drawing.Color.DodgerBlue;
            this.btnMinimizar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(158)))));
            this.btnMinimizar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnMinimizar.BorderRadius = 0;
            this.btnMinimizar.ButtonText = "";
            this.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMinimizar.DisabledColor = System.Drawing.Color.Gray;
            this.btnMinimizar.Iconcolor = System.Drawing.Color.Transparent;
            this.btnMinimizar.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnMinimizar.Iconimage")));
            this.btnMinimizar.Iconimage_right = null;
            this.btnMinimizar.Iconimage_right_Selected = null;
            this.btnMinimizar.Iconimage_Selected = null;
            this.btnMinimizar.IconMarginLeft = 0;
            this.btnMinimizar.IconMarginRight = 0;
            this.btnMinimizar.IconRightVisible = true;
            this.btnMinimizar.IconRightZoom = 0D;
            this.btnMinimizar.IconVisible = true;
            this.btnMinimizar.IconZoom = 60D;
            this.btnMinimizar.IsTab = false;
            this.btnMinimizar.Location = new System.Drawing.Point(724, 2);
            this.btnMinimizar.Name = "btnMinimizar";
            this.btnMinimizar.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(66)))), ((int)(((byte)(158)))));
            this.btnMinimizar.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btnMinimizar.OnHoverTextColor = System.Drawing.Color.White;
            this.btnMinimizar.selected = false;
            this.btnMinimizar.Size = new System.Drawing.Size(35, 33);
            this.btnMinimizar.TabIndex = 1;
            this.btnMinimizar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMinimizar.Textcolor = System.Drawing.Color.White;
            this.btnMinimizar.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimizar.Click += new System.EventHandler(this.btnMinimizar_Click);
            // 
            // bnfControl
            // 
            this.bnfControl.BackColor = System.Drawing.SystemColors.Control;
            this.bnfControl.BorderRadius = 7;
            this.bnfControl.BottomSahddow = true;
            this.bnfControl.color = System.Drawing.Color.RoyalBlue;
            this.bnfControl.Controls.Add(this.lblEtapa);
            this.bnfControl.Controls.Add(this.lblTotal);
            this.bnfControl.Controls.Add(this.btnAvisoDetenida);
            this.bnfControl.Controls.Add(this.btnAvisoActiva);
            this.bnfControl.Controls.Add(this.label12);
            this.bnfControl.Controls.Add(this.lblProceso);
            this.bnfControl.Controls.Add(this.label10);
            this.bnfControl.Controls.Add(this.label9);
            this.bnfControl.Controls.Add(this.label8);
            this.bnfControl.Controls.Add(this.label7);
            this.bnfControl.Controls.Add(this.BarProceso);
            this.bnfControl.Controls.Add(this.label6);
            this.bnfControl.Controls.Add(this.barTotal);
            this.bnfControl.LeftSahddow = true;
            this.bnfControl.Location = new System.Drawing.Point(292, 255);
            this.bnfControl.Name = "bnfControl";
            this.bnfControl.RightSahddow = true;
            this.bnfControl.ShadowDepth = 40;
            this.bnfControl.Size = new System.Drawing.Size(467, 284);
            this.bnfControl.TabIndex = 2;
            this.bnfControl.Paint += new System.Windows.Forms.PaintEventHandler(this.bnfCard_Paint);
            // 
            // lblEtapa
            // 
            this.lblEtapa.AutoSize = true;
            this.lblEtapa.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEtapa.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblEtapa.Location = new System.Drawing.Point(373, 104);
            this.lblEtapa.Name = "lblEtapa";
            this.lblEtapa.Size = new System.Drawing.Size(0, 17);
            this.lblEtapa.TabIndex = 23;
            this.lblEtapa.Click += new System.EventHandler(this.label11_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.ForeColor = System.Drawing.Color.MidnightBlue;
            this.lblTotal.Location = new System.Drawing.Point(373, 169);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(0, 17);
            this.lblTotal.TabIndex = 5;
            // 
            // btnAvisoDetenida
            // 
            this.btnAvisoDetenida.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAvisoDetenida.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAvisoDetenida.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAvisoDetenida.BorderRadius = 7;
            this.btnAvisoDetenida.ButtonText = "Detenida";
            this.btnAvisoDetenida.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAvisoDetenida.DisabledColor = System.Drawing.Color.Red;
            this.btnAvisoDetenida.Enabled = false;
            this.btnAvisoDetenida.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAvisoDetenida.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAvisoDetenida.Iconimage")));
            this.btnAvisoDetenida.Iconimage_right = null;
            this.btnAvisoDetenida.Iconimage_right_Selected = null;
            this.btnAvisoDetenida.Iconimage_Selected = null;
            this.btnAvisoDetenida.IconMarginLeft = 0;
            this.btnAvisoDetenida.IconMarginRight = 0;
            this.btnAvisoDetenida.IconRightVisible = true;
            this.btnAvisoDetenida.IconRightZoom = 0D;
            this.btnAvisoDetenida.IconVisible = true;
            this.btnAvisoDetenida.IconZoom = 60D;
            this.btnAvisoDetenida.IsTab = false;
            this.btnAvisoDetenida.Location = new System.Drawing.Point(28, 191);
            this.btnAvisoDetenida.Name = "btnAvisoDetenida";
            this.btnAvisoDetenida.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAvisoDetenida.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnAvisoDetenida.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAvisoDetenida.selected = false;
            this.btnAvisoDetenida.Size = new System.Drawing.Size(146, 35);
            this.btnAvisoDetenida.TabIndex = 22;
            this.btnAvisoDetenida.Text = "Detenida";
            this.btnAvisoDetenida.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAvisoDetenida.Textcolor = System.Drawing.Color.White;
            this.btnAvisoDetenida.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnAvisoActiva
            // 
            this.btnAvisoActiva.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAvisoActiva.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAvisoActiva.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnAvisoActiva.BorderRadius = 7;
            this.btnAvisoActiva.ButtonText = "Activa";
            this.btnAvisoActiva.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAvisoActiva.DisabledColor = System.Drawing.Color.LimeGreen;
            this.btnAvisoActiva.Enabled = false;
            this.btnAvisoActiva.Iconcolor = System.Drawing.Color.Transparent;
            this.btnAvisoActiva.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnAvisoActiva.Iconimage")));
            this.btnAvisoActiva.Iconimage_right = null;
            this.btnAvisoActiva.Iconimage_right_Selected = null;
            this.btnAvisoActiva.Iconimage_Selected = null;
            this.btnAvisoActiva.IconMarginLeft = 0;
            this.btnAvisoActiva.IconMarginRight = 0;
            this.btnAvisoActiva.IconRightVisible = true;
            this.btnAvisoActiva.IconRightZoom = 0D;
            this.btnAvisoActiva.IconVisible = true;
            this.btnAvisoActiva.IconZoom = 60D;
            this.btnAvisoActiva.IsTab = false;
            this.btnAvisoActiva.Location = new System.Drawing.Point(28, 191);
            this.btnAvisoActiva.Name = "btnAvisoActiva";
            this.btnAvisoActiva.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.btnAvisoActiva.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(129)))), ((int)(((byte)(77)))));
            this.btnAvisoActiva.OnHoverTextColor = System.Drawing.Color.White;
            this.btnAvisoActiva.selected = false;
            this.btnAvisoActiva.Size = new System.Drawing.Size(146, 35);
            this.btnAvisoActiva.TabIndex = 21;
            this.btnAvisoActiva.Text = "Activa";
            this.btnAvisoActiva.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnAvisoActiva.Textcolor = System.Drawing.Color.White;
            this.btnAvisoActiva.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.Control;
            this.label12.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label12.Location = new System.Drawing.Point(25, 170);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(152, 17);
            this.label12.TabIndex = 20;
            this.label12.Text = "Estatus de la maquina";
            // 
            // lblProceso
            // 
            this.lblProceso.AutoSize = true;
            this.lblProceso.BackColor = System.Drawing.SystemColors.Control;
            this.lblProceso.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblProceso.Font = new System.Drawing.Font("Myanmar Text", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProceso.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblProceso.Location = new System.Drawing.Point(31, 120);
            this.lblProceso.Name = "lblProceso";
            this.lblProceso.Size = new System.Drawing.Size(119, 34);
            this.lblProceso.TabIndex = 17;
            this.lblProceso.Text = "Sin proceso";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.Control;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label10.Location = new System.Drawing.Point(26, 103);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 17);
            this.label10.TabIndex = 16;
            this.label10.Text = "Proceso actual";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label9.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label9.Location = new System.Drawing.Point(3, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(286, 37);
            this.label9.TabIndex = 13;
            this.label9.Text = "Información de producción";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.Control;
            this.label8.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label8.Location = new System.Drawing.Point(233, 170);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(99, 17);
            this.label8.TabIndex = 15;
            this.label8.Text = "Progreso total";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.Control;
            this.label7.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label7.Location = new System.Drawing.Point(233, 105);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 17);
            this.label7.TabIndex = 13;
            this.label7.Text = "Progreso por etapa";
            // 
            // BarProceso
            // 
            this.BarProceso.BackColor = System.Drawing.Color.LightGray;
            this.BarProceso.BorderRadius = 5;
            this.BarProceso.Location = new System.Drawing.Point(236, 127);
            this.BarProceso.MaximumValue = 100;
            this.BarProceso.Name = "BarProceso";
            this.BarProceso.ProgressColor = System.Drawing.Color.LightSeaGreen;
            this.BarProceso.Size = new System.Drawing.Size(170, 32);
            this.BarProceso.TabIndex = 14;
            this.BarProceso.Value = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.Control;
            this.label6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label6.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Teal;
            this.label6.Location = new System.Drawing.Point(130, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(178, 37);
            this.label6.TabIndex = 13;
            this.label6.Text = "Panel de control";
            // 
            // barTotal
            // 
            this.barTotal.BackColor = System.Drawing.Color.LightGray;
            this.barTotal.BorderRadius = 5;
            this.barTotal.Location = new System.Drawing.Point(236, 192);
            this.barTotal.MaximumValue = 100;
            this.barTotal.Name = "barTotal";
            this.barTotal.ProgressColor = System.Drawing.Color.LimeGreen;
            this.barTotal.Size = new System.Drawing.Size(170, 32);
            this.barTotal.TabIndex = 1;
            this.barTotal.Value = 0;
            // 
            // btnTipo
            // 
            this.btnTipo.BackColor = System.Drawing.Color.IndianRed;
            this.btnTipo.BorderRadius = 5;
            this.btnTipo.BottomSahddow = true;
            this.btnTipo.color = System.Drawing.Color.Red;
            this.btnTipo.Controls.Add(this.label1);
            this.btnTipo.Controls.Add(this.lblMarias);
            this.btnTipo.Controls.Add(this.checkMarias);
            this.btnTipo.Controls.Add(this.lblBombon);
            this.btnTipo.Controls.Add(this.checkBombon);
            this.btnTipo.Controls.Add(this.lblChispas);
            this.btnTipo.Controls.Add(this.checkChispas);
            this.btnTipo.LeftSahddow = false;
            this.btnTipo.Location = new System.Drawing.Point(30, 137);
            this.btnTipo.Name = "btnTipo";
            this.btnTipo.RightSahddow = true;
            this.btnTipo.ShadowDepth = 20;
            this.btnTipo.Size = new System.Drawing.Size(729, 87);
            this.btnTipo.TabIndex = 3;
            this.btnTipo.Paint += new System.Windows.Forms.PaintEventHandler(this.btnTipo_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(34, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(210, 20);
            this.label1.TabIndex = 7;
            this.label1.Text = "Selecciona el tipo de galleta:";
            // 
            // lblMarias
            // 
            this.lblMarias.AutoSize = true;
            this.lblMarias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblMarias.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarias.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblMarias.Location = new System.Drawing.Point(595, 43);
            this.lblMarias.Name = "lblMarias";
            this.lblMarias.Size = new System.Drawing.Size(82, 37);
            this.lblMarias.TabIndex = 6;
            this.lblMarias.Text = "Marías";
            this.lblMarias.Click += new System.EventHandler(this.lblMarias_Click);
            // 
            // checkMarias
            // 
            this.checkMarias.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.checkMarias.ChechedOffColor = System.Drawing.Color.White;
            this.checkMarias.Checked = true;
            this.checkMarias.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.checkMarias.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkMarias.ForeColor = System.Drawing.Color.White;
            this.checkMarias.Location = new System.Drawing.Point(572, 48);
            this.checkMarias.Name = "checkMarias";
            this.checkMarias.Size = new System.Drawing.Size(20, 20);
            this.checkMarias.TabIndex = 5;
            this.checkMarias.OnChange += new System.EventHandler(this.checkMarias_OnChange);
            // 
            // lblBombon
            // 
            this.lblBombon.AutoSize = true;
            this.lblBombon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblBombon.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBombon.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblBombon.Location = new System.Drawing.Point(314, 43);
            this.lblBombon.Name = "lblBombon";
            this.lblBombon.Size = new System.Drawing.Size(131, 37);
            this.lblBombon.TabIndex = 4;
            this.lblBombon.Text = "Bombonera";
            this.lblBombon.Click += new System.EventHandler(this.lblBombon_Click);
            // 
            // checkBombon
            // 
            this.checkBombon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.checkBombon.ChechedOffColor = System.Drawing.Color.White;
            this.checkBombon.Checked = true;
            this.checkBombon.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.checkBombon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBombon.ForeColor = System.Drawing.Color.White;
            this.checkBombon.Location = new System.Drawing.Point(291, 48);
            this.checkBombon.Name = "checkBombon";
            this.checkBombon.Size = new System.Drawing.Size(20, 20);
            this.checkBombon.TabIndex = 3;
            this.checkBombon.OnChange += new System.EventHandler(this.checkBombon_OnChange);
            // 
            // lblChispas
            // 
            this.lblChispas.AutoSize = true;
            this.lblChispas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblChispas.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChispas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblChispas.Location = new System.Drawing.Point(63, 43);
            this.lblChispas.Name = "lblChispas";
            this.lblChispas.Size = new System.Drawing.Size(91, 37);
            this.lblChispas.TabIndex = 2;
            this.lblChispas.Text = "Chispas";
            this.lblChispas.Click += new System.EventHandler(this.lblChispas_Click);
            // 
            // checkChispas
            // 
            this.checkChispas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.checkChispas.ChechedOffColor = System.Drawing.Color.White;
            this.checkChispas.Checked = true;
            this.checkChispas.CheckedOnColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(205)))), ((int)(((byte)(117)))));
            this.checkChispas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkChispas.ForeColor = System.Drawing.Color.White;
            this.checkChispas.Location = new System.Drawing.Point(40, 48);
            this.checkChispas.Name = "checkChispas";
            this.checkChispas.Size = new System.Drawing.Size(20, 20);
            this.checkChispas.TabIndex = 1;
            this.checkChispas.OnChange += new System.EventHandler(this.checkChispas_OnChange);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Nirmala UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkSlateBlue;
            this.label3.Location = new System.Drawing.Point(259, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(316, 86);
            this.label3.TabIndex = 4;
            this.label3.Text = "Gallemax";
            // 
            // txtTiempo
            // 
            this.txtTiempo.BackColor = System.Drawing.SystemColors.Control;
            this.txtTiempo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtTiempo.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtTiempo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtTiempo.HintForeColor = System.Drawing.Color.Gray;
            this.txtTiempo.HintText = "Tiempo (s)";
            this.txtTiempo.isPassword = false;
            this.txtTiempo.LineFocusedColor = System.Drawing.Color.IndianRed;
            this.txtTiempo.LineIdleColor = System.Drawing.Color.Gray;
            this.txtTiempo.LineMouseHoverColor = System.Drawing.Color.Red;
            this.txtTiempo.LineThickness = 3;
            this.txtTiempo.Location = new System.Drawing.Point(27, 88);
            this.txtTiempo.Margin = new System.Windows.Forms.Padding(4);
            this.txtTiempo.Name = "txtTiempo";
            this.txtTiempo.Size = new System.Drawing.Size(197, 32);
            this.txtTiempo.TabIndex = 5;
            this.txtTiempo.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtTiempo.OnValueChanged += new System.EventHandler(this.txtTiempo_OnValueChanged);
            this.txtTiempo.Click += new System.EventHandler(this.txtTiempo_Click);
            this.txtTiempo.MouseHover += new System.EventHandler(this.txtTiempo_MouseHover);
            // 
            // txtCantidad
            // 
            this.txtCantidad.BackColor = System.Drawing.SystemColors.Control;
            this.txtCantidad.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txtCantidad.Font = new System.Drawing.Font("Century Gothic", 9.75F);
            this.txtCantidad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtCantidad.HintForeColor = System.Drawing.Color.Gray;
            this.txtCantidad.HintText = "Cantidad (piezas)";
            this.txtCantidad.isPassword = false;
            this.txtCantidad.LineFocusedColor = System.Drawing.Color.IndianRed;
            this.txtCantidad.LineIdleColor = System.Drawing.Color.Gray;
            this.txtCantidad.LineMouseHoverColor = System.Drawing.Color.Red;
            this.txtCantidad.LineThickness = 3;
            this.txtCantidad.Location = new System.Drawing.Point(27, 153);
            this.txtCantidad.Margin = new System.Windows.Forms.Padding(4);
            this.txtCantidad.Name = "txtCantidad";
            this.txtCantidad.Size = new System.Drawing.Size(197, 32);
            this.txtCantidad.TabIndex = 6;
            this.txtCantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.txtCantidad.Click += new System.EventHandler(this.txtCantidad_Click);
            this.txtCantidad.MouseHover += new System.EventHandler(this.txtCantidad_MouseHover);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.Control;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label2.Location = new System.Drawing.Point(22, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(220, 17);
            this.label2.TabIndex = 8;
            this.label2.Text = "Ingresa el tiempo de producción";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.Control;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label4.Location = new System.Drawing.Point(24, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(205, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Ingresa la cantidad a producir";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.Control;
            this.label5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label5.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(3, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 37);
            this.label5.TabIndex = 8;
            this.label5.Text = "Producción";
            // 
            // bnfDatos
            // 
            this.bnfDatos.BackColor = System.Drawing.SystemColors.Control;
            this.bnfDatos.BorderRadius = 7;
            this.bnfDatos.BottomSahddow = true;
            this.bnfDatos.color = System.Drawing.Color.LimeGreen;
            this.bnfDatos.Controls.Add(this.btnReinicio);
            this.bnfDatos.Controls.Add(this.btnParo);
            this.bnfDatos.Controls.Add(this.btnInicio);
            this.bnfDatos.Controls.Add(this.label5);
            this.bnfDatos.Controls.Add(this.label4);
            this.bnfDatos.Controls.Add(this.label2);
            this.bnfDatos.Controls.Add(this.txtCantidad);
            this.bnfDatos.Controls.Add(this.txtTiempo);
            this.bnfDatos.LeftSahddow = true;
            this.bnfDatos.Location = new System.Drawing.Point(41, 255);
            this.bnfDatos.Name = "bnfDatos";
            this.bnfDatos.RightSahddow = true;
            this.bnfDatos.ShadowDepth = 40;
            this.bnfDatos.Size = new System.Drawing.Size(245, 284);
            this.bnfDatos.TabIndex = 3;
            // 
            // btnReinicio
            // 
            this.btnReinicio.Activecolor = System.Drawing.Color.LightSteelBlue;
            this.btnReinicio.BackColor = System.Drawing.SystemColors.Control;
            this.btnReinicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnReinicio.BorderRadius = 6;
            this.btnReinicio.ButtonText = "";
            this.btnReinicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReinicio.DisabledColor = System.Drawing.Color.Gray;
            this.btnReinicio.Iconcolor = System.Drawing.Color.Transparent;
            this.btnReinicio.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnReinicio.Iconimage")));
            this.btnReinicio.Iconimage_right = null;
            this.btnReinicio.Iconimage_right_Selected = null;
            this.btnReinicio.Iconimage_Selected = null;
            this.btnReinicio.IconMarginLeft = 0;
            this.btnReinicio.IconMarginRight = 0;
            this.btnReinicio.IconRightVisible = true;
            this.btnReinicio.IconRightZoom = 0D;
            this.btnReinicio.IconVisible = true;
            this.btnReinicio.IconZoom = 70D;
            this.btnReinicio.IsTab = false;
            this.btnReinicio.Location = new System.Drawing.Point(171, 213);
            this.btnReinicio.Name = "btnReinicio";
            this.btnReinicio.Normalcolor = System.Drawing.SystemColors.Control;
            this.btnReinicio.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btnReinicio.OnHoverTextColor = System.Drawing.Color.White;
            this.btnReinicio.selected = false;
            this.btnReinicio.Size = new System.Drawing.Size(53, 48);
            this.btnReinicio.TabIndex = 12;
            this.btnReinicio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnReinicio.Textcolor = System.Drawing.Color.White;
            this.btnReinicio.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReinicio.Click += new System.EventHandler(this.btnReinicio_Click);
            // 
            // btnParo
            // 
            this.btnParo.Activecolor = System.Drawing.Color.IndianRed;
            this.btnParo.BackColor = System.Drawing.SystemColors.Control;
            this.btnParo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnParo.BorderRadius = 6;
            this.btnParo.ButtonText = "";
            this.btnParo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnParo.DisabledColor = System.Drawing.Color.Gray;
            this.btnParo.Iconcolor = System.Drawing.Color.Transparent;
            this.btnParo.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnParo.Iconimage")));
            this.btnParo.Iconimage_right = null;
            this.btnParo.Iconimage_right_Selected = null;
            this.btnParo.Iconimage_Selected = null;
            this.btnParo.IconMarginLeft = 0;
            this.btnParo.IconMarginRight = 0;
            this.btnParo.IconRightVisible = true;
            this.btnParo.IconRightZoom = 0D;
            this.btnParo.IconVisible = true;
            this.btnParo.IconZoom = 80D;
            this.btnParo.IsTab = false;
            this.btnParo.Location = new System.Drawing.Point(99, 213);
            this.btnParo.Name = "btnParo";
            this.btnParo.Normalcolor = System.Drawing.SystemColors.Control;
            this.btnParo.OnHovercolor = System.Drawing.Color.LightSalmon;
            this.btnParo.OnHoverTextColor = System.Drawing.Color.White;
            this.btnParo.selected = false;
            this.btnParo.Size = new System.Drawing.Size(53, 48);
            this.btnParo.TabIndex = 11;
            this.btnParo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnParo.Textcolor = System.Drawing.Color.White;
            this.btnParo.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnParo.Click += new System.EventHandler(this.btnParo_Click);
            // 
            // btnInicio
            // 
            this.btnInicio.Activecolor = System.Drawing.Color.LightGreen;
            this.btnInicio.BackColor = System.Drawing.SystemColors.Control;
            this.btnInicio.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnInicio.BorderRadius = 6;
            this.btnInicio.ButtonText = "";
            this.btnInicio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInicio.DisabledColor = System.Drawing.Color.Gray;
            this.btnInicio.Iconcolor = System.Drawing.Color.Transparent;
            this.btnInicio.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnInicio.Iconimage")));
            this.btnInicio.Iconimage_right = null;
            this.btnInicio.Iconimage_right_Selected = null;
            this.btnInicio.Iconimage_Selected = null;
            this.btnInicio.IconMarginLeft = 0;
            this.btnInicio.IconMarginRight = 0;
            this.btnInicio.IconRightVisible = true;
            this.btnInicio.IconRightZoom = 0D;
            this.btnInicio.IconVisible = true;
            this.btnInicio.IconZoom = 80D;
            this.btnInicio.IsTab = false;
            this.btnInicio.Location = new System.Drawing.Point(29, 213);
            this.btnInicio.Name = "btnInicio";
            this.btnInicio.Normalcolor = System.Drawing.SystemColors.Control;
            this.btnInicio.OnHovercolor = System.Drawing.Color.LightGreen;
            this.btnInicio.OnHoverTextColor = System.Drawing.Color.White;
            this.btnInicio.selected = false;
            this.btnInicio.Size = new System.Drawing.Size(53, 48);
            this.btnInicio.TabIndex = 10;
            this.btnInicio.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInicio.Textcolor = System.Drawing.Color.White;
            this.btnInicio.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnInicio.Click += new System.EventHandler(this.btnInicio_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(800, 569);
            this.ControlBox = false;
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnTipo);
            this.Controls.Add(this.bnfControl);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.bnfDatos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Principal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.Load += new System.EventHandler(this.Principal_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.bnfControl.ResumeLayout(false);
            this.bnfControl.PerformLayout();
            this.btnTipo.ResumeLayout(false);
            this.btnTipo.PerformLayout();
            this.bnfDatos.ResumeLayout(false);
            this.bnfDatos.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuFlatButton btnCerrar;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuFlatButton btnMinimizar;
        private Bunifu.Framework.UI.BunifuCards bnfControl;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuCards btnTipo;
        private System.Windows.Forms.Label lblMarias;
        private Bunifu.Framework.UI.BunifuCheckbox checkMarias;
        private System.Windows.Forms.Label lblBombon;
        private Bunifu.Framework.UI.BunifuCheckbox checkBombon;
        private System.Windows.Forms.Label lblChispas;
        private Bunifu.Framework.UI.BunifuCheckbox checkChispas;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtTiempo;
        private System.Windows.Forms.Label label1;
        private Bunifu.Framework.UI.BunifuMaterialTextbox txtCantidad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuCards bnfDatos;
        private Bunifu.Framework.UI.BunifuFlatButton btnReinicio;
        private Bunifu.Framework.UI.BunifuFlatButton btnParo;
        private Bunifu.Framework.UI.BunifuFlatButton btnInicio;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuProgressBar BarProceso;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuProgressBar barTotal;
        private System.Windows.Forms.Label lblProceso;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblTotal;
        private Bunifu.Framework.UI.BunifuFlatButton btnAvisoActiva;
        private Bunifu.Framework.UI.BunifuFlatButton btnAvisoDetenida;
        private System.Windows.Forms.Label lblEtapa;
    }
}