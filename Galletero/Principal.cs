﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Timers;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Galletero
{
    public partial class Principal : Form
    {
        Stopwatch stpwatch = new Stopwatch();
        TimeSpan timeSpan; 
        int tipoChispa, tipoMarias, tipoBombon;
        int contadorProceso = 0,  listas=0;
        int stop = 0;
        public Principal()
        {
            InitializeComponent();
           
        }
        void ChecharTipo()
        {  
           if(checkChispas.Checked || tipoChispa==1)           
                bnfDatos.Enabled = true;
 
           else if(checkBombon.Checked || tipoBombon == 1)           
                bnfDatos.Enabled = true;

            else if (checkMarias.Checked || tipoMarias == 1)         
                bnfDatos.Enabled = true;
                  
        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblChispas_Click(object sender, EventArgs e)
        {
            tipoChispa = 1;
            tipoBombon = 0;
            tipoMarias = 0;
           checkChispas.Checked = true;
            checkBombon.Checked = false;
            checkMarias.Checked = false;
            ChecharTipo();
        }

        private void lblBombon_Click(object sender, EventArgs e)
        {
            tipoChispa = 0;
            tipoBombon = 1;
            tipoMarias = 0;
            checkChispas.Checked = false;
           checkBombon.Checked = true;
            checkMarias.Checked = false;
            ChecharTipo();
        }

        private void lblMarias_Click(object sender, EventArgs e)
        {
            tipoChispa = 0;
            tipoBombon = 0;
            tipoMarias = 1;
            checkChispas.Checked = false;
            checkBombon.Checked = false;
           checkMarias.Checked = true;
            ChecharTipo();
        }

        private void bunifuFlatButton4_Click(object sender, EventArgs e)
        {

        }

        private void checkChispas_OnChange(object sender, EventArgs e)
        {
            tipoChispa = 1;
            tipoBombon = 0;
            tipoMarias = 0;
            checkChispas.Checked = true;
            checkBombon.Checked = false;
            checkMarias.Checked = false;
            ChecharTipo();
        }

        private void checkBombon_OnChange(object sender, EventArgs e)
        {
            tipoChispa = 0;
            tipoBombon = 1;
            tipoMarias = 0;
            checkChispas.Checked = false;
            checkBombon.Checked = true;
            checkMarias.Checked = false;
            ChecharTipo();
        }

        private void btnTipo_Paint(object sender, PaintEventArgs e)
        {

        }

        private void checkMarias_OnChange(object sender, EventArgs e)
        {
            tipoChispa = 0;
            tipoBombon = 0;
            tipoMarias = 1;
            checkChispas.Checked = false;
            checkBombon.Checked = false;
            checkMarias.Checked = true;
            ChecharTipo();
        }

        private void bnfCard_Paint(object sender, PaintEventArgs e)
        {

        }
        int ChecarCantidad()
        {
            decimal a = 0;
            if (txtTiempo.Text=="")
            {
                a = decimal.Parse(txtCantidad.Text);
            }
           else if(txtCantidad.Text=="")
            {
                if (checkBombon.Checked || tipoBombon==1)
                {                  
                    a = Math.Floor(decimal.Parse(txtTiempo.Text) / 23);
                }
                else if (checkChispas.Checked || tipoChispa == 1)
                {                  
                    a = Math.Floor(decimal.Parse(txtTiempo.Text) / 29);
                }
                else if (checkMarias.Checked || tipoMarias == 1)
                {                   
                    a = Math.Floor(decimal.Parse(txtTiempo.Text) / 20);
                }
                
            }
            return Decimal.ToInt32(a);
        }
        int a = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
                 int cantidadTotal = ChecarCantidad();
                barTotal.MaximumValue = cantidadTotal;
                if (a <= cantidadTotal)
                {
                    timeSpan = new TimeSpan(0, 0, 0, 0, (int)stpwatch.ElapsedMilliseconds);
                    InicioProceso(timeSpan.Seconds);                
                    barTotal.Value = +a;
                    lblTotal.Text = "" + a + "" + "/" + "" + cantidadTotal + "";
                    if (a == cantidadTotal)
                    {
                        stpwatch.Reset();
                        timer1.Enabled = false;
                        btnAvisoActiva.Visible = false;
                        btnAvisoDetenida.Visible = true;
                        lblProceso.Text = "Proceso Terminado";
                        lblProceso.ForeColor = Color.FromArgb(228, 46, 18);
                        LimpiarConsola();
                    }
                }

           
        
        }
           
        void LimpiarConsola()
        {
            tipoBombon = 0;
            tipoChispa = 0;
            tipoMarias = 0;
            checkBombon.Checked = false;
            checkChispas.Checked = false;
            checkMarias.Checked = false;
            txtCantidad.ResetText();
            txtTiempo.ResetText();
            stop = 0;
            contadorProceso = 0;
            a = 0;
        }
        void ResetearBarras()
        {
            timer1.Enabled = false;
            stpwatch.Reset();

            barTotal.ResetText();
            barTotal.Value = 0;
            BarProceso.ResetText();
            BarProceso.Value = 0;
            btnAvisoActiva.Visible = true;
            btnAvisoDetenida.Visible = false;
            a = 0;
        }
     
        private void btnInicio_Click(object sender, EventArgs e)
        {
           
            if(checkMarias.Checked==false && tipoMarias==0 && checkChispas.Checked == false && tipoChispa == 0 && checkBombon.Checked == false && tipoBombon == 0)
            {
                MessageBox.Show("Seleccione un tipo de galleta");
            }
            else
            {
                if (txtCantidad.Text == "" && txtTiempo.Text == "")
                {
                    MessageBox.Show("Ingrese los datos para producción");
                }
                else
                {
                    try
                    {
                        
                      //  ResetearBarras();
                        timer1.Enabled = true;
                        stpwatch.Start();
                        
                    }
                    catch { MessageBox.Show("Ingrese valores enteros unicamente"); }
                }
               
            }
           
        }

        private void btnParo_Click(object sender, EventArgs e)
        {
            btnAvisoActiva.Visible = false;
            btnAvisoDetenida.Visible = true;
            timer1.Enabled = false;
            stpwatch.Stop();
            stop = 1;
        }

        private void txtTiempo_Click(object sender, EventArgs e)
        {
           
        }

        private void btnReinicio_Click(object sender, EventArgs e)
        {

            if (stop == 1)
            {
                MessageBox.Show("A continuación, el proceso se reiniciará");
              
                barTotal.Value = 0;
                BarProceso.Value = 0;               
                stop = 0;
                a = 0;
                ResetearBarras();
                timer1.Enabled = true;
                stpwatch.Start();
                btnAvisoActiva.Visible = true;
                btnAvisoDetenida.Visible = false;
            }
            else
                MessageBox.Show("Necesita detener la maquina");
            
        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void txtTiempo_OnValueChanged(object sender, EventArgs e)
        {
           
        }

        private void txtCantidad_MouseHover(object sender, EventArgs e)
        {
           
        }

        private void txtTiempo_MouseHover(object sender, EventArgs e)
        {
            
        }

            private void txtCantidad_Click(object sender, EventArgs e)
        {
            txtCantidad.Focus();
            txtTiempo.Text = "";
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            bnfControl.Enabled = true;
            bnfDatos.Enabled = false;
            checkBombon.Checked = false;
            checkMarias.Checked = false;
            checkChispas.Checked = false;
           
        }

        void InicioProceso( int tiempoProceso)
        {
           
            btnAvisoActiva.Visible = true;
            btnAvisoDetenida.Visible = false;
            stop = 0;
            stpwatch.Start();
            timer1.Enabled = true;
            if (checkBombon.Checked == true || tipoBombon == 1)
                {
                    
                    if (contadorProceso == 0)
                    {
                        BarProceso.MaximumValue = 8;
                        lblProceso.Text = "Batido";
                        if (tiempoProceso <= 8)
                        {
                            BarProceso.Value = tiempoProceso;
                            lblEtapa.Text= ""+tiempoProceso+"" + "/" + "8";
                        }
                        else
                        {
                            stpwatch.Reset();
                            timer1.Enabled = false;
                            contadorProceso = 1;

                        }
                        timer1.Enabled = true;
                        stpwatch.Start();
                    }
                    else
                    {
                        BarProceso.MaximumValue = 15;
                        lblProceso.Text = "Horneado";
                        if (tiempoProceso <= 15)
                        {
                            BarProceso.Value = tiempoProceso;
                            lblEtapa.Text = "" + tiempoProceso + "" + "/" + "15";
                    }
                        else
                        {
                            BarProceso.Value = 0;
                            timer1.Enabled = false;
                            stpwatch.Reset();
                            a++;
                            contadorProceso = 0;
                           

                        }
                    }
                }

                else if (checkChispas.Checked == true || tipoChispa == 1)
                {
                if (contadorProceso == 0)
                {

                    BarProceso.MaximumValue = 9;
                    lblProceso.Text = "Batido";

                    if (tiempoProceso <= 9)
                    {
                        BarProceso.Value = tiempoProceso;
                        lblEtapa.Text = "" + tiempoProceso + "" + "/" + "9";
                    }
                    else
                    {
                        stpwatch.Reset();
                        timer1.Enabled = false;
                        contadorProceso = 1;

                    }
                    stpwatch.Start();
                    timer1.Enabled = true;
                }
                else
                {

                    BarProceso.MaximumValue = 20;
                    lblProceso.Text = "Horneado ";
                    if (tiempoProceso <= 20)
                    {
                        BarProceso.Value = tiempoProceso;
                        lblEtapa.Text = "" + tiempoProceso + "" + "/" + "20";
                    }
                    else
                    {
                        BarProceso.Value = 0;
                        timer1.Enabled = false;
                        stpwatch.Reset();
                        a++;
                        contadorProceso = 0;
                    }
                    stpwatch.Start();
                    timer1.Enabled = true;

                }
            }
                else if (checkMarias.Checked == true || tipoMarias == 1)
                {
                    if (contadorProceso == 0)
                    {
                        
                        BarProceso.MaximumValue = 5;
                        lblProceso.Text = "Batido";
                        
                        if (tiempoProceso <= 5)
                        {
                            BarProceso.Value = tiempoProceso;
                        lblEtapa.Text = "" + tiempoProceso + "" + "/" + "5";
                    }
                        else
                        {
                            stpwatch.Reset();
                            timer1.Enabled = false;
                            contadorProceso = 1;

                        }
                    stpwatch.Start();
                    timer1.Enabled = true;
                    }               
                    else
                    {
                    
                        BarProceso.MaximumValue = 10;
                        lblProceso.Text = "Horneado ";
                        if (tiempoProceso <= 10)
                        {
                            BarProceso.Value = tiempoProceso;
                        lblEtapa.Text = "" + tiempoProceso + "" + "/" + "10";
                    }
                        else
                        {
                            BarProceso.Value = 0;                       
                            timer1.Enabled = false;
                            stpwatch.Reset();
                            a++;
                            contadorProceso = 0;
                        }
                    stpwatch.Start();
                    timer1.Enabled = true;

                    }
                }
                else
                {
                    MessageBox.Show("Seleccione un tipo de galletazo");
                }

            }
        }
    }

